﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Entities
{
    public class Address
    {
        public string Street { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public int DoorNumber { get; set; }
        public int ZipCode { get; set; }
        public string Phone { get; set; }

        public Address()
        {

        }
    }
}
